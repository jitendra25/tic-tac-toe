let value = 0;
let matrix = [[-1, -1, -1], [-1, -1, -1], [-1, -1, -1]];
const container = document.getElementById("cont");
function check() {
    var i, j;
    let text = "Winner is : Player "
    for (i = 0; i < 3; i++) {
        var temp = matrix[i][0];
        var temp1 = matrix[0][i];

        if (temp == matrix[i][1] && temp == matrix[i][2] && temp != -1) {
            text += (1 - value);
            document.getElementById("winner").innerHTML = text;
            var id = i * 3;
            document.getElementById(id).style.background = "blue";
            document.getElementById(id + 1).style.background = "blue";
            document.getElementById(id + 2).style.background = "blue";
            return;
        }
        if (temp1 == matrix[1][i] && temp1 == matrix[2][i] && temp1 != -1) {
            text += (-1) * (value - 1);
            document.getElementById("winner").innerHTML = text;
            var id = i;
            document.getElementById(id).style.background = "blue";
            document.getElementById(id + 3).style.background = "blue";
            document.getElementById(id + 6).style.background = "blue";
            return;
        }
        if (i == 0 && temp1 == matrix[1][1] && temp1 == matrix[2][2] && temp1 != -1) {
            text += (-1) * (value - 1);
            document.getElementById("winner").innerHTML = text;
            var id = i;
            document.getElementById(0).style.background = "blue";
            document.getElementById(4).style.background = "blue";
            document.getElementById(8).style.background = "blue";
            return;
        }
        if (i == 2 && temp1 == matrix[1][1] && temp1 == matrix[2][0] && temp1 != -1) {
            text += (-1) * (value - 1);
            document.getElementById("winner").innerHTML = text;
            document.getElementById(2).style.background = "blue";
            document.getElementById(4).style.background = "blue";
            document.getElementById(6).style.background = "blue";
            return;
        }
    }
    var total = 0;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            if (matrix[i][j] != -1) {
                total++;
            }
        }
    }
    if (total == 9) {
        document.getElementById("winner").innerHTML = "Match Tied";
    }

}
function fill() {
    let i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            var id = (i * 3 + j);
            if (matrix[i][j] == 1) {
                document.getElementById(id).getElementsByTagName("h2")[0].innerText = 1;
            }
            else if (matrix[i][j] == 0) {
                document.getElementById(id).getElementsByTagName("h2")[0].innerText = 0;
            }
            else {
                document.getElementById(id).getElementsByTagName("h2")[0].innerText = "";
            }
        }
    }
    let text = "Player " + value;
    document.getElementsByTagName("span")[0].innerText = text;

}
container.querySelectorAll("div").forEach(item => {
    item.addEventListener("click", event => {
        var id = item.id;
        if (matrix[Math.floor(id / 3)][id % 3] != -1) {
            document.getElementById("error").style.display = "inline";
            return;
        }
        document.getElementById("error").style.display = "none";
        matrix[Math.floor(id / 3)][id % 3] = value;
        value++;
        if (value == 2)
            value = 0;

        fill();
        check();
    })
});
function reset() {
    var i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            matrix[i][j] = -1;
            var id = i * 3 + j;
            document.getElementById(id).style.backgroundColor = "white";
        }
    }
    document.getElementById("error").style.display = "none";
    document.getElementById("winner").innerHTML = "";
    value = 0;
    fill();
}